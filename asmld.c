#include <stdio.h>
#include <sys/mman.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/ptrace.h>
#include <stdbool.h>

#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64) || defined(_M_X64) || defined(_M_AMD64)
	#define ARCH "x86-64"
#elif defined(i386) || defined(__i386) || defined(__i386__) || defined(__IA32__) || defined(_M_IX86) || defined(_X86_) || defined(__X86__)
	#define ARCH "x86"
#else
	#define ARCH "UNKNOWN"
#endif

#define DEBUGGER_FEATURES
//#undef DEBUGGER_FEATURES

size_t get_file_size(FILE *f)
{
	size_t s;
	fseek(f,0,SEEK_END);
	s = ftell(f);
	fseek(f,0,SEEK_SET);
	return s;
}

bool is_debugger_present()
{
	if(ptrace(PTRACE_TRACEME, 0, NULL, 0) == -1)
		return true;
	return false;
}

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		puts("usage: asmld <file.bin>");
		return -1;
	}

	FILE *file = NULL;
	void *code;
	size_t size = 0;
	bool dbg = is_debugger_present();

	file = fopen(argv[1], "rb");
	if(file == NULL)
	{
		perror(argv[1]);
		return -2;
	}

	size = get_file_size(file);
	if(size == -1)
	{
		perror("Could not read file");
		return -3;
	}

	code = mmap(NULL, size, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE|MAP_ANON, -1, 0);
	if(code == MAP_FAILED)
	{
		perror("Allocation failed");
		return -4;
	}

	if(fread(code,size,1,file) == 0)
	{
		puts("Could not load code to memory");
		return -5;
	}

	fclose(file);

	printf("Architecture: %s\nAllocated \x1b[32m%u bytes\x1b[0m of memory\nCode loaded in memory at address: \x1b[32m%p\x1b[0m\n", ARCH, (uint32_t)size, code);
	
	#ifdef DEBUGGER_FEATURES
		if(dbg)
		{
			puts("\x1b[31m[!] Debugger detected!\n    Sending SIGTRAP before executing loaded code.\x1b[0m");
			asm("int3":::);
		}
	#endif	

	((void(*)(void))code)();

	#ifdef DEBUGGER_FEATURES
		if(dbg)
		{
			puts("\x1b[31m[!] Code ended it execution.\n    Sending SIGTRAP.\x1b[0m");
			asm("int3":::);
		}
	#endif

	if(munmap(code,size) == -1)
	{
		perror("Deallocation failed");
		return -6;
	}
	return 0;
}
