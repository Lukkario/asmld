[bits 64]
global _start
_start:

    ; step 1, create socket
    ; socket(AF_INTE, SOCK_STREAM, IPPROTO_IP)
    mov rax, 41		; rax = sys_socket
    mov rsi, 1		; rsi = SOCK_STREAM
    mov rdi, 2		; rdi = AF_INET
    mov rdx, 0		; rdx = IPPROTO_IP
    syscall
    
push rax
mov rax, 0x1
mov rdi, 0x1
lea rsi, [rel created_socket]
mov rdx, 25
syscall
pop rax
    ; step 2, bind to port 1234 
    ; bind(s, {AF_INET,1234,INADDR_ANY}, 16)
    mov rdi, rax        	 ; rdi = socket
    lea rsi, [rel sockaddr]	 ; rbx = &sockaddr
    mov rdx, 16			 ; rdx = sizeof(sockaddr)
    mov rax, 49 		 ; rax = sys_bind
    syscall

push rdi
mov rax, 0x1
mov rdi, 0x1
lea rsi, [rel bind_socket]
mov rdx, 22
syscall
pop rdi
    ; step 3, listen
    ; listen(socket, blocking queue);
    xor rsi, rsi	; rsi = 0 - blocking queue
    mov rax, 50		; rax = sys_listen
    syscall

push rdi
mov rax, 0x1
mov rdi, 0x1
lea rsi, [rel listen_socket]
mov rdx, 13
syscall
pop rdi

    ; step 4, accept connections
    ; accept(socket, 0, 0);

    mov rax, 43		; rax = sys_accept
    mov rsi, 0
    mov rdx, 0
    syscall

    xchg rax, rdi
    push rax 		; push main socket addres on stack

    ; step 5, send Hello World! through socket
    mov rax, 1		; sys_write
    lea rsi, [rel msg]	; rsi = *msg
    mov rdx, 12		; rdx = sizeof(msg)
    syscall

    ; step 6, close client socket
    pop rdi	; rdi = client socket from stack
    mov rax, 3	; rax = sys_close
    syscall

    ; step 7, close main socket
    pop rdi	; rdi = main socket from stack
    mov rax, 3	; rax = sys_close
    syscall

    ; step 8, close application
    mov rax, 60		; rax = sys_exit
    mov rdi, 0x0	; rdi = error_code
    syscall

sockaddr:
dw 2		; AF_INET
dw 0xD204	; PORT 1234
dw 0		; INADDR_ANY

sock_client:
dw 0
dw 0
dw 0

msg:
db "Hello world!", 0xA

created_socket:
db "Socket has been created!", 0xA

bind_socket:
db "Socket has been bind!", 0xA

listen_socket:
db "Listening...", 0xA
