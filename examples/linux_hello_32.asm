[bits 32]

mov eax, 4
mov ebx, 1
call get_eip
get_eip:
pop ecx
lea ecx, [ecx+msg1-0xF]
mov edx, 0xD
int 0x80
jmp hello

hello:
mov eax, 4
lea ecx, [ecx+0xD]
int 0x80
ret

msg1: db "Hello World1", 0xA
msg2: db "Hello World2", 0xA
