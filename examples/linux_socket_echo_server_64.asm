[bits 64]

global _start
_start:

    ; step 1, create socket
    ; socket(AF_INTE, SOCK_STREAM, IPPROTO_IP)
    mov rax, 41		; rax = sys_socket
    mov rsi, 1		; rsi = SOCK_STREAM
    mov rdi, 2		; rdi = AF_INET
    mov rdx, 0		; rdx = IPPROTO_IP
    syscall
    
push rax
mov rax, 0x1
mov rdi, 0x1
lea rsi, [rel created_socket]
lea rdx, [rel created_socket_len]
syscall
pop rax

    ; step 2, bind to port 1234 
    ; bind(s, {AF_INET,1234,INADDR_ANY}, 16)
    mov rdi, rax        	 ; rdi = socket
    lea rsi, [rel sockaddr]	 ; rbx = &sockaddr
    mov rdx, 16			 ; rdx = sizeof(sockaddr)
    mov rax, 49 		 ; rax = sys_bind
    syscall

push rdi
mov rax, 0x1
mov rdi, 0x1
lea rsi, [rel bind_socket]
lea rdx, [rel bind_socket_len]
syscall
pop rdi

    ; step 3, listen
    ; listen(socket, blocking queue);
    xor rsi, rsi	; rsi = 0 - blocking queue
    mov rax, 50		; rax = sys_listen
    syscall

push rdi
mov rax, 0x1
mov rdi, 0x1
lea rsi, [rel listen_socket]
lea rdx, [rel listen_socket_len]
syscall
pop rdi

    ; step 4, accept connections
    ; accept(socket, 0, 0);
    mov rax, 43		; rax = sys_accept
    mov rsi, 0
    mov rdx, 0
    syscall

    xchg rax, rdi
    push rax 		; push main socket addres on stack

push rdi
mov rax, 0x1
mov rdi, 0x1
lea rsi, [rel connected]
lea rdx, [rel connected_len]
syscall
pop rdi

    mov rax, 0
    lea rsi, [rel mes]
    mov rdx, 256
    syscall

    ; step 5, send Hello World! through socket
    mov rax, 1		; sys_write
    lea rsi, [rel mes]	; rsi = *msg
    mov rdx, 12		; rdx = sizeof(msg)
    syscall

mov rax, 0x1
mov rdi, 0x1
lea rsi, [rel closing]
lea rdx, [rel closing_len]
syscall

    ; step 6, close client socket
    mov rax, 3	; rax = sys_close
    syscall

    ; step 7, close main socket
    pop rdi	; rdi = main socket from stack
    mov rax, 3	; rax = sys_close
    syscall

    ; step 8, close application
    mov rax, 60		; rax = sys_exit
    mov rdi, 0x0	; rdi = error_code
    syscall

sockaddr:
dw 2		; AF_INET
dw 0xD204	; PORT 1234
resw 2		; INADDR_ANY

created_socket:
db "Socket has been created!", 0xA
created_socket_len: equ $ - created_socket

bind_socket:
db "Socket has been bind!", 0xA
bind_socket_len: equ $ - bind_socket

listen_socket:
db "Listening...", 0xA
listen_socket_len: equ $ - listen_socket

connected:
db "Connected!", 0xA
connected_len: equ $ - connected

closing:
db "Closing...", 0xA
closing_len: equ $ - closing

mes:
resb 256
